package ru.tsc.felofyanov.tm.integration.rest;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import ru.tsc.felofyanov.tm.marker.IntegrationCategory;
import ru.tsc.felofyanov.tm.model.Project;
import ru.tsc.felofyanov.tm.model.Result;

import java.net.HttpCookie;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Category(IntegrationCategory.class)
public class ProjectRestEndpointTest {

    @NotNull
    private static final String ROOT = "http://localhost:8080/api/projects/";

    @NotNull
    private static final HttpHeaders header = new HttpHeaders();

    @Nullable
    private static String sessionId;

    @NotNull
    private final Project alpha = new Project("Test-1");

    @NotNull
    private final Project beta = new Project("Test-2");

    @NotNull
    private final Project gamma = new Project("Test-3");

    @BeforeClass
    public static void beforeClass() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = "http://localhost:8080/api/auth/login?login=test&password=test";
        header.setContentType(MediaType.APPLICATION_JSON);
        @NotNull final ResponseEntity<Result> response =
                restTemplate.postForEntity(url, new HttpEntity<>(""), Result.class);

        Assert.assertEquals(200, response.getStatusCodeValue());
        Assert.assertNotNull(response.getBody());
        Assert.assertTrue(response.getBody().getSuccess());

        @NotNull final HttpHeaders responseHeaders = response.getHeaders();
        @NotNull final List<HttpCookie> cookies = java.net.HttpCookie.parse(
                responseHeaders.getFirst(HttpHeaders.SET_COOKIE)
        );
        sessionId = cookies.stream()
                .filter(item -> "JSESSIONID".equals(item.getName()))
                .findFirst().get().getValue();

        Assert.assertNotNull(sessionId);
        header.put(HttpHeaders.COOKIE, Arrays.asList("JSESSIONID=" + sessionId));
        header.setContentType(MediaType.APPLICATION_JSON);
    }

    private static <T> ResponseEntity<T> sendRequest(
            @NotNull final String url,
            @NotNull final HttpMethod method,
            @NotNull final HttpEntity httpEntity,
            @NotNull final Class<T> responseType
    ) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        return restTemplate.exchange(url, method, httpEntity, responseType);
    }

    @AfterClass
    public static void logout() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = "http://localhost:8080/api/auth/logout";
        sendRequest(url, HttpMethod.POST, new HttpEntity(header), Result.class);
    }

    @Before
    public void init() {
        @NotNull final String url = ROOT + "save";
        sendRequest(url, HttpMethod.POST, new HttpEntity(alpha, header), Project.class);
        sendRequest(url, HttpMethod.POST, new HttpEntity(beta, header), Project.class);
    }

    @After
    public void clear() {
        @NotNull final String url = ROOT + "clear";
        sendRequest(url, HttpMethod.DELETE, new HttpEntity(header), Project.class);
    }

    @Test
    public void saveTest() {
        @NotNull final String url = ROOT + "save";
        @NotNull final ResponseEntity<Project> response
                = sendRequest(url, HttpMethod.POST, new HttpEntity(gamma, header), Project.class);
        Assert.assertEquals(response.getStatusCode(), HttpStatus.OK);

        @Nullable final Project test = response.getBody();
        Assert.assertNotNull(test);
        Assert.assertEquals(test.getId(), gamma.getId());
    }

    @Test
    public void saveAllTest() {
        @NotNull final String url = ROOT + "saveAll";
        @NotNull final List<Project> projects = new ArrayList<>();
        projects.add(alpha);
        projects.add(beta);
        projects.add(gamma);
        Assert.assertNotNull(projects);

        @NotNull final ResponseEntity<Project[]> response
                = sendRequest(url, HttpMethod.POST, new HttpEntity(projects, header), Project[].class);

        Assert.assertEquals(response.getStatusCode(), HttpStatus.OK);
        Assert.assertNotNull(response.getBody());
        Assert.assertEquals(3, response.getBody().length);
    }

    @Test
    public void countTest() {
        @NotNull final String url = ROOT + "count";
        @NotNull final ResponseEntity<Long> response
                = sendRequest(url, HttpMethod.GET, new HttpEntity(header), Long.class);
        Assert.assertEquals(response.getStatusCode(), HttpStatus.OK);
        Assert.assertNotNull(response.getBody());
        @NotNull final Long count = response.getBody();
        Assert.assertEquals(2, count.longValue());
    }

    @Test
    public void existsByIdTest() {
        @NotNull String url = ROOT + "existsById/" + gamma.getId();
        @NotNull ResponseEntity<Boolean> response
                = sendRequest(url, HttpMethod.GET, new HttpEntity(header), Boolean.class);
        Assert.assertEquals(response.getStatusCode(), HttpStatus.OK);
        Assert.assertNotNull(response.getBody());
        Assert.assertFalse(response.getBody());

        url = ROOT + "existsById/" + beta.getId();
        response = sendRequest(url, HttpMethod.GET, new HttpEntity(header), Boolean.class);
        Assert.assertEquals(response.getStatusCode(), HttpStatus.OK);
        Assert.assertNotNull(response.getBody());
        Assert.assertTrue(response.getBody());
    }

    @Test
    public void findAllTest() {
        @NotNull final String url = ROOT + "findAll";
        @NotNull final ResponseEntity<Project[]> response
                = sendRequest(url, HttpMethod.GET, new HttpEntity(header), Project[].class);
        Assert.assertEquals(response.getStatusCode(), HttpStatus.OK);
        Assert.assertNotNull(response.getBody());
        Assert.assertEquals(2, response.getBody().length);
    }

    @Test
    public void findByIdTest() {
        @NotNull final String url = ROOT + "findById/" + alpha.getId();
        @NotNull final ResponseEntity<Project> response
                = sendRequest(url, HttpMethod.GET, new HttpEntity(header), Project.class);
        Assert.assertEquals(response.getStatusCode(), HttpStatus.OK);
        Assert.assertNotNull(response.getBody());
        Assert.assertEquals(alpha.getId(), response.getBody().getId());
    }

    @Test
    public void deleteTest() {
        @NotNull final String url = ROOT + "delete";
        @NotNull final ResponseEntity<Project> response
                = sendRequest(url, HttpMethod.POST, new HttpEntity(beta, header), Project.class);
        Assert.assertEquals(response.getStatusCode(), HttpStatus.OK);

        @NotNull final String urlFind = ROOT + "findById/" + beta.getId();
        @NotNull final ResponseEntity<Project> responseFind
                = sendRequest(urlFind, HttpMethod.GET, new HttpEntity(header), Project.class);
        Assert.assertEquals(responseFind.getStatusCode(), HttpStatus.OK);
        Assert.assertNull(response.getBody());
    }

    @Test
    public void deleteAllTest() {
        @NotNull final String url = ROOT + "deleteAll";

        @NotNull final List<Project> projects = new ArrayList<>();
        projects.add(alpha);
        projects.add(beta);
        projects.add(gamma);
        Assert.assertNotNull(projects);

        @NotNull final ResponseEntity<Project> response
                = sendRequest(url, HttpMethod.POST, new HttpEntity(projects, header), Project.class);
        Assert.assertEquals(response.getStatusCode(), HttpStatus.OK);

        @NotNull final ResponseEntity<Long> responseCount
                = sendRequest(ROOT + "count", HttpMethod.GET, new HttpEntity(header), Long.class);
        Assert.assertEquals(response.getStatusCode(), HttpStatus.OK);
        Assert.assertNotNull(responseCount.getBody());
        Assert.assertEquals(0, responseCount.getBody().longValue());
    }

    @Test
    public void deleteByIdTest() {
        @NotNull final String url = ROOT + "deleteById/" + beta.getId();
        @NotNull final ResponseEntity<Project> response
                = sendRequest(url, HttpMethod.DELETE, new HttpEntity(header), Project.class);
        Assert.assertEquals(response.getStatusCode(), HttpStatus.OK);

        @NotNull final String urlFind = ROOT + "findById/" + beta.getId();
        @NotNull final ResponseEntity<Project> responseFind
                = sendRequest(urlFind, HttpMethod.GET, new HttpEntity(header), Project.class);
        Assert.assertEquals(responseFind.getStatusCode(), HttpStatus.OK);
        Assert.assertNull(response.getBody());
    }
}
