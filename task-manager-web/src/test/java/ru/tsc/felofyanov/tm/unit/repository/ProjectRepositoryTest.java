package ru.tsc.felofyanov.tm.unit.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.felofyanov.tm.api.repository.IProjectRepository;
import ru.tsc.felofyanov.tm.config.ApplicationConfiguration;
import ru.tsc.felofyanov.tm.config.WebApplicationConfiguration;
import ru.tsc.felofyanov.tm.marker.WebUnitCategory;
import ru.tsc.felofyanov.tm.model.Project;
import ru.tsc.felofyanov.tm.util.UserUtil;

@Transactional
@WebAppConfiguration
@Category(WebUnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebApplicationConfiguration.class, ApplicationConfiguration.class})
public class ProjectRepositoryTest {

    @NotNull
    private final Project alpha = new Project("Test-1");

    @NotNull
    private final Project beta = new Project("Test-2");

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private IProjectRepository repository;

    @Before
    public void init() {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("test", "test");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        alpha.setUserId(UserUtil.getUserId());
        beta.setUserId(UserUtil.getUserId());
        repository.save(alpha);
        repository.save(beta);
    }

    @After
    public void clean() {
        repository.deleteByUserId(UserUtil.getUserId());
    }

    @Test
    public void findAllTest() {
        Assert.assertNotNull(repository.findAllByUserId(UserUtil.getUserId()));
        Assert.assertEquals(2, repository.findAllByUserId(UserUtil.getUserId()).size());
    }

    @Test
    public void findFirstByUserIdAndIdTest() {
        Assert.assertNotNull(repository.findFirstByUserIdAndId(UserUtil.getUserId(), alpha.getId()));
        @Nullable final Project test = repository.findFirstByUserIdAndId(UserUtil.getUserId(), alpha.getId());
        Assert.assertNotNull(test);
        Assert.assertEquals(alpha.getId(), test.getId());
    }

    @Test
    public void deleteByUserIdTest() {
        repository.deleteByUserId(UserUtil.getUserId());
        Assert.assertEquals(0, repository.findAllByUserId(UserUtil.getUserId()).size());
    }

    @Test
    public void deleteByUserIdAndIdTest() {
        repository.deleteByUserIdAndId(UserUtil.getUserId(), alpha.getId());
        Assert.assertNull(repository.findFirstByUserIdAndId(UserUtil.getUserId(), alpha.getId()));
        Assert.assertEquals(1, repository.findAllByUserId(UserUtil.getUserId()).size());
    }

    @Test
    public void countTest() {
        Assert.assertEquals(2, repository.countByUserId(UserUtil.getUserId()));
    }
}
