package ru.tsc.felofyanov.tm.client;

import org.jetbrains.annotations.NotNull;
import ru.tsc.felofyanov.tm.api.endpoint.IAuthEndpoint;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Service;
import java.net.MalformedURLException;
import java.net.URL;

public class AuthSoapEndpointClient {

    public static IAuthEndpoint getInstance(@NotNull final String baseURL) throws MalformedURLException {
        @NotNull final String wsdl = baseURL + "/services/AuthEndpoint?wsdl";
        @NotNull final URL url = new URL(wsdl);
        @NotNull final String lp = "AuthEndpointService";
        @NotNull final String ns = "http://endpoint.tm.felofyanov.tsc.ru/";
        @NotNull final QName name = new QName(ns, lp);
        @NotNull final IAuthEndpoint result = Service.create(url, name).getPort(IAuthEndpoint.class);
        @NotNull final BindingProvider bindingProvider = (BindingProvider) result;
        bindingProvider.getRequestContext().put(BindingProvider.SESSION_MAINTAIN_PROPERTY, true);
        return result;
    }
}
