package ru.tsc.felofyanov.tm.unit.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.tsc.felofyanov.tm.marker.BootUnitCategory;
import ru.tsc.felofyanov.tm.model.Task;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@WebAppConfiguration
@Category(BootUnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class TaskEndpointTest {

    @NotNull
    private static final String PROJECT_URL = "https://localhost:8080/api/tasks/";

    @NotNull
    private final Task alpha = new Task("Test-1");

    @NotNull
    private final Task beta = new Task("Test-2");

    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Before
    public void init() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("test", "test");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        save(alpha);
        save(beta);
    }

    @After
    @SneakyThrows
    public void clean() {
        mockMvc.perform(MockMvcRequestBuilders.delete(PROJECT_URL + "clear")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void saveTest() {
        save(alpha);
        @Nullable final Task task = findById(alpha.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(task.getId(), alpha.getId());
    }

    @Test
    public void saveAllTest() {
        @NotNull final List<Task> projects = new ArrayList<>();
        projects.add(alpha);
        projects.add(beta);
        save(projects);
        Assert.assertNotNull(findById(alpha.getId()));
        Assert.assertNotNull(findById(beta.getId()));
    }

    @Test
    public void findAllTest() {
        Assert.assertNotNull(findAll());
        Assert.assertEquals(2, findAll().size());
    }

    @Test
    @SneakyThrows
    public void deleteTest() {
        @NotNull final String url = PROJECT_URL + "delete";
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(beta);
        mockMvc.perform(MockMvcRequestBuilders.post(url).content(json).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
        Assert.assertEquals(1, findAll().size());
    }

    @Test
    @SneakyThrows
    public void deleteAllTest() {
        @NotNull final String url = PROJECT_URL + "deleteAll";
        @NotNull final List<Task> projects = new ArrayList<>();
        projects.add(alpha);
        projects.add(beta);
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(projects);
        mockMvc.perform(MockMvcRequestBuilders.post(url).content(json)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
        Assert.assertEquals(0, findAll().size());
    }

    @Test
    @SneakyThrows
    public void deleteById() {
        @NotNull final String url = PROJECT_URL + "deleteById/" + alpha.getId();
        mockMvc.perform(MockMvcRequestBuilders.delete(url).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
        Assert.assertNull(findById(alpha.getId()));
    }

    @Test
    @SneakyThrows
    public void clearTest() {
        @NotNull final String url = PROJECT_URL + "clear";
        mockMvc.perform(MockMvcRequestBuilders.delete(url).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
        Assert.assertEquals(0, findAll().size());
    }

    @SneakyThrows
    private void save(@NotNull final Task task) {
        @NotNull final String url = PROJECT_URL + "save";
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(task);
        mockMvc.perform(MockMvcRequestBuilders.post(url)
                .content(json)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @SneakyThrows
    private void save(@NotNull final List<Task> projects) {
        @NotNull final String url = PROJECT_URL + "saveAll";
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(projects);
        mockMvc.perform(MockMvcRequestBuilders.post(url).content(json).contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @SneakyThrows
    private Task findById(@NotNull final String id) {
        @NotNull final String url = PROJECT_URL + "findById/" + id;
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        if (json.isEmpty()) return null;
        @NotNull ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, Task.class);
    }

    @SneakyThrows
    private List<Task> findAll() {
        @NotNull final String url = PROJECT_URL + "findAll";
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull ObjectMapper objectMapper = new ObjectMapper();
        return Arrays.asList(objectMapper.readValue(json, Task[].class));
    }
}
