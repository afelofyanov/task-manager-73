package ru.tsc.felofyanov.tm.integration.soap;

import lombok.SneakyThrows;
import org.apache.cxf.helpers.CastUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.http.HttpHeaders;
import ru.tsc.felofyanov.tm.api.endpoint.IAuthEndpoint;
import ru.tsc.felofyanov.tm.api.endpoint.IProjectEndpoint;
import ru.tsc.felofyanov.tm.client.AuthSoapEndpointClient;
import ru.tsc.felofyanov.tm.client.ProjectSoapEndpointClient;
import ru.tsc.felofyanov.tm.marker.BootIntegrationCategory;
import ru.tsc.felofyanov.tm.model.Project;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;
import java.util.*;

@Category(BootIntegrationCategory.class)
public class ProjectSoapEndpointTest {

    @NotNull
    private static final String ROOT = "http://localhost:8080";

    @NotNull
    private static IAuthEndpoint authEndpoint;

    @NotNull
    private static IProjectEndpoint projectEndpoint;

    @NotNull
    private final Project alpha = new Project("Test-1");

    @NotNull
    private final Project beta = new Project("Test-2");

    @NotNull
    private final Project gamma = new Project("Test-3");

    @BeforeClass
    @SneakyThrows
    public static void beforeClass() {
        authEndpoint = AuthSoapEndpointClient.getInstance(ROOT);
        Assert.assertTrue(authEndpoint.login("test", "test").getSuccess());
        projectEndpoint = ProjectSoapEndpointClient.getInstance(ROOT);

        @NotNull final BindingProvider authBindingProvider = (BindingProvider) authEndpoint;
        @NotNull final BindingProvider projectBindingProvider = (BindingProvider) projectEndpoint;
        @NotNull Map<String, List<String>> headers = CastUtils.cast(
                (Map) authBindingProvider.getResponseContext().get(MessageContext.HTTP_RESPONSE_HEADERS)
        );
        if (headers == null) headers = new HashMap<String, List<String>>();

        @NotNull final Object cookieValue = headers.get(HttpHeaders.SET_COOKIE);
        @NotNull final List<String> cookies = (List<String>) cookieValue;
        headers.put("Cookie", Collections.singletonList(cookies.get(0)));
        projectBindingProvider.getRequestContext().put(MessageContext.HTTP_REQUEST_HEADERS, headers);
    }

    @AfterClass
    public static void logout() {
        authEndpoint.logout();
    }

    @Before
    public void init() {
        projectEndpoint.save(alpha);
        projectEndpoint.save(beta);
    }

    @After
    public void clear() {
        projectEndpoint.clear();
    }

    @Test
    public void saveTest() {
        projectEndpoint.save(gamma);
        @Nullable final Project findProject = projectEndpoint.findById(gamma.getId());
        Assert.assertNotNull(findProject);
        Assert.assertEquals(gamma.getId(), findProject.getId());
    }

    @Test
    public void saveAllTest() {
        @NotNull final List<Project> projects = new ArrayList<>();
        projects.add(alpha);
        projects.add(gamma);
        projectEndpoint.saveAll(projects);
        @Nullable final List<Project> findProject = projectEndpoint.findAll();
        Assert.assertNotNull(findProject);
        Assert.assertEquals(3, findProject.size());
    }

    @Test
    public void findAllTest() {
        @Nullable final List<Project> test = projectEndpoint.findAll();
        Assert.assertNotNull(test);
        Assert.assertEquals(2, test.size());
    }

    @Test
    public void findByIdTest() {
        @Nullable final Project test = projectEndpoint.findById(beta.getId());
        Assert.assertNotNull(test);
        Assert.assertEquals(beta.getId(), test.getId());
    }

    @Test
    public void deleteTest() {
        projectEndpoint.delete(alpha);
        @Nullable final Project test = projectEndpoint.findById(alpha.getId());
        Assert.assertNull(test);
    }

    @Test
    public void deleteAllTest() {
        @NotNull final List<Project> projects = new ArrayList<>();

        projects.add(alpha);
        projects.add(gamma);
        projectEndpoint.saveAll(projects);

        Assert.assertEquals(3, projectEndpoint.findAll().size());
        projectEndpoint.deleteAll(projects);
        Assert.assertEquals(1, projectEndpoint.findAll().size());
    }

    @Test
    public void deleteByIdTest() {
        projectEndpoint.deleteById(alpha.getId());
        @Nullable final Project test = projectEndpoint.findById(alpha.getId());
        Assert.assertNull(test);
        Assert.assertEquals(1, projectEndpoint.findAll().size());
    }

    @Test
    public void countTest() {
        Assert.assertEquals(2, projectEndpoint.count());
        Assert.assertEquals(projectEndpoint.count(), projectEndpoint.findAll().size());
    }

    @Test
    public void existByIdTest() {
        Assert.assertTrue(projectEndpoint.existsById(alpha.getId()));
        Assert.assertFalse(projectEndpoint.existsById(gamma.getId()));
    }
}
